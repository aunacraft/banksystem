package net.aunacraft.banksystem;

import net.aunacraft.api.AunaAPI;
import net.aunacraft.api.backend.message.MessageService;
import net.aunacraft.api.bukkit.location.SpigotLocationProvider;
import net.aunacraft.api.database.DatabaseHandler;
import net.aunacraft.api.database.config.impl.SpigotDatabaseConfig;
import net.aunacraft.api.player.AunaPlayer;
import net.aunacraft.banksystem.level.impl.GeneralLevelHandler;
import net.aunacraft.banksystem.level.impl.TresorLevelHandler;
import net.aunacraft.banksystem.listeners.JoinListener;
import net.aunacraft.banksystem.listeners.QuitListener;
import net.aunacraft.banksystem.npc.NPCManager;
import net.aunacraft.banksystem.npc.impl.BankManageNPC;
import net.aunacraft.banksystem.npc.impl.BankSecurityNPC;
import net.aunacraft.banksystem.npc.impl.BankServiceNPC;
import net.aunacraft.banksystem.transaction.TransactionManager;
import net.aunacraft.banksystem.tresor.TresorRegionProcessor;
import net.aunacraft.banksystem.tresor.item.ItemTresorHandler;
import net.aunacraft.banksystem.utils.Interests;
import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import java.time.LocalDateTime;
import java.util.Arrays;

public class BankSystem extends JavaPlugin {

    // FIELDS
    private static BankSystem instance;
    private MessageService messageService;
    private DatabaseHandler databaseHandler;
    private TransactionManager transactionManager;
    private GeneralLevelHandler generalLevel;
    private TresorLevelHandler tresorLevel;
    private SpigotLocationProvider spigotLocationProvider;
    private NPCManager npcManager;
    private ItemTresorHandler itemTresorHandler;

    public static BankSystem getInstance() {
        return instance;
    }

    // GETTER
    public MessageService getMessageService() {
        return messageService;
    }

    public DatabaseHandler getDatabaseHandler() {
        return databaseHandler;
    }

    public TransactionManager getTransactionManager() {
        return transactionManager;
    }

    public GeneralLevelHandler getGeneralLevel() {
        return generalLevel;
    }

    public TresorLevelHandler getTresorLevel() {
        return tresorLevel;
    }

    public SpigotLocationProvider getSpigotLocationProvider() {
        return spigotLocationProvider;
    }

    public ItemTresorHandler getItemTresorHandler() {
        return itemTresorHandler;
    }

    @Override
    public void onEnable() {
        instance = this;
        this.messageService = AunaAPI.getApi().getMessageService();
        this.generalLevel = new GeneralLevelHandler();
        this.tresorLevel = new TresorLevelHandler();
        this.spigotLocationProvider = new SpigotLocationProvider(this, true);
        this.npcManager = new NPCManager(Arrays.asList(new BankServiceNPC(), new BankManageNPC(), new BankSecurityNPC()));
        this.itemTresorHandler = new ItemTresorHandler();
        new TresorRegionProcessor();
        ////////////  BUKKIT REGISTRATION (EVENTS/COMMANDS)  /////////////
        new QuitListener();
        new JoinListener();
        ////////////  CE Api REGISTRATION etc. /////////////
        AunaAPI.getApi().getMessageService().applyPrefix("bank", "bank.prefix");
        AunaAPI.getApi().registerLocationProvider(this.spigotLocationProvider);
        this.databaseHandler = new DatabaseHandler(new SpigotDatabaseConfig(this));
        this.transactionManager = new TransactionManager(this);
        this.databaseHandler.createBuilder("CREATE TABLE IF NOT EXISTS bank_tresorInv(uuid TEXT, jsonContent TEXT);").updateSync();
        this.databaseHandler.createBuilder("CREATE TABLE IF NOT EXISTS bank_generalBank(uuid TEXT, bankLevel INT, tresorLevel INT);").updateSync();
        this.getServer().getLogger().info("BankSystem enabled!");

        ////////////  INFINITY TIMERS /////////////////
        this.getServer().getScheduler().runTaskTimer(this, () -> {
            LocalDateTime timeNow = LocalDateTime.now();
            if (timeNow.getHour() == 12 && timeNow.getMinute() == 0)
                Bukkit.getOnlinePlayers().forEach(spigotPlayer -> {
                    AunaPlayer player = AunaAPI.getApi().getPlayer(spigotPlayer.getUniqueId());
                    AunaAPI.getApi().getCoinService().getBankCoins(player.getUuid(), bankCoins ->
                            AunaAPI.getApi().getCoinService().setBankCoins(player.getUuid(), Interests.addInterests(bankCoins), () -> {
                            }));
                });
        }, 0, 20 * 5);
    }

    @Override
    public void onDisable() {
        this.getServer().getLogger().info("Disabled BankSystem!");
    }
}
