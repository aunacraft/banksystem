package net.aunacraft.banksystem.gui;

import net.aunacraft.api.AunaAPI;
import net.aunacraft.api.gui.Gui;
import net.aunacraft.api.gui.GuiInventory;
import net.aunacraft.api.player.AunaPlayer;
import net.aunacraft.api.util.ItemBuilder;
import net.aunacraft.banksystem.BankSystem;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;

public class BuyBankAccountGui extends Gui {

    private final AunaPlayer player;

    public BuyBankAccountGui(AunaPlayer player) {
        super(BankSystem.getInstance().getMessageService().getMessageForPlayer(player.toBukkitPlayer(), "bank.gui.buyacc.title"), 27);
        this.player = player;
    }

    @Override
    public void initInventory(Player player, GuiInventory guiInventory) {
        guiInventory.fill(new ItemBuilder(Material.GRAY_STAINED_GLASS_PANE).setDisplayName(" ").build());
        guiInventory.setItem(13, new ItemBuilder(Material.EMERALD).setDisplayName(
                BankSystem.getInstance().getMessageService().getMessageForPlayer(player, "bank.gui.buyacc.itemname")).build(), inventoryClickEvent -> {
            AunaAPI.getApi().getCoinService().buyTransaction(this.player.getUuid(), 100, able -> {
                if (able) {
                    player.sendMessage(BankSystem.getInstance().getMessageService().getMessageForPlayer(player, "bank.gui.buyacc.succesfull"));
                    Bukkit.getScheduler().scheduleSyncDelayedTask(BankSystem.getInstance(), () -> new IndexGui(this.player).open(player));
                    BankSystem.getInstance().getGeneralLevel().setLevel(this.player, 1);
                    /*BankSystem.getInstance().getDatabaseHandler().createBuilder("UPDATE generalBank SET bankLevel = ? WHERE uuid = ?")
                            .addObjects(1, AunaAPI.getApi().getRealUUID(player).toString()).updateAsync();*/
                } else
                    player.sendMessage(BankSystem.getInstance().getMessageService().getMessageForPlayer(player, "bank.gui.buyacc.nomoney"));
            });
        });
    }
}
