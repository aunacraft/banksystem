package net.aunacraft.banksystem.gui;

import net.aunacraft.api.AunaAPI;
import net.aunacraft.api.gui.Gui;
import net.aunacraft.api.gui.GuiInventory;
import net.aunacraft.api.player.AunaPlayer;
import net.aunacraft.api.util.ItemBuilder;
import net.aunacraft.banksystem.BankSystem;
import net.aunacraft.banksystem.transaction.Transaction;
import net.aunacraft.banksystem.transaction.impl.DepositTransaction;
import org.bukkit.Material;
import org.bukkit.entity.Player;

public class ConfirmGui extends Gui {

    private final AunaPlayer player;
    private final Transaction transaction;

    public ConfirmGui(AunaPlayer opener, Transaction transaction) {
        super(transaction.getConfirmString(), 27);
        this.transaction = transaction;
        this.player = opener;
    }

    @Override
    public void initInventory(Player player, GuiInventory guiInventory) {
        guiInventory.fill(new ItemBuilder(Material.GRAY_STAINED_GLASS_PANE).setDisplayName(" ").build());
        guiInventory.setItem(0, new ItemBuilder(Material.YELLOW_STAINED_GLASS_PANE).setDisplayName(" ").build());
        guiInventory.setItem(8, new ItemBuilder(Material.YELLOW_STAINED_GLASS_PANE).setDisplayName(" ").build());
        guiInventory.setItem(18, new ItemBuilder(Material.YELLOW_STAINED_GLASS_PANE).setDisplayName(" ").build());
        guiInventory.setItem(26, new ItemBuilder(Material.YELLOW_STAINED_GLASS_PANE).setDisplayName(" ").build());
        guiInventory.setItem(13, new ItemBuilder(Material.GOLD_NUGGET).setDisplayName(this.transaction.getConfirmString().replaceAll("&", "§")).build());
        guiInventory.setItem(11, new ItemBuilder(Material.RED_BANNER)
                .setDisplayName(BankSystem.getInstance().getMessageService().getMessageForPlayer(player, "bank.gui.ablehnen")).build(), inventoryClickEvent -> {
            BankSystem.getInstance().getTransactionManager().getPlayerTransactionCache().get(this.player).cancel();
            BankSystem.getInstance().getTransactionManager().getPlayerTransactionCache().remove(this.player);
            new IndexGui(this.player).open(this.player.toBukkitPlayer());
        });
        guiInventory.setItem(15, new ItemBuilder(Material.GREEN_BANNER)
                .setDisplayName(BankSystem.getInstance().getMessageService().getMessageForPlayer(player, "bank.gui.annehmen")).build(), inventoryClickEvent -> {
            //Transaction transaction = BankSystem.getInstance().getTransactionManager().getPlayerTransactionCache().get(AunaAPI.getApi().getPlayer(player.getUniqueId()));
            if (transaction instanceof DepositTransaction) {
                transaction.next();
                AunaAPI.getApi().getCoinService().bankPayIn(transaction.getOwner().getUuid(), transaction.getAmount(), able -> {
                    if (able) {
                        player.sendMessage(BankSystem.getInstance().getMessageService().getMessageForPlayer(player, "bank.gui.confirm.succesmsg"));
                        BankSystem.getInstance().getTransactionManager().getPlayerTransactionCache().remove(transaction.getOwner());
                    } else
                        player.sendMessage(BankSystem.getInstance().getMessageService().getMessageForPlayer(player, "bank.gui.buyacc.nomoney"));
                });
            } else {
                transaction.next();
                AunaAPI.getApi().getCoinService().bankPayOut(transaction.getOwner().getUuid(), transaction.getAmount(), able -> {
                    if (able) {
                        player.sendMessage(BankSystem.getInstance().getMessageService().getMessageForPlayer(player, "bank.gui.confirm.succesmsg"));
                        BankSystem.getInstance().getTransactionManager().getPlayerTransactionCache().remove(transaction.getOwner());
                    } else
                        player.sendMessage(BankSystem.getInstance().getMessageService().getMessageForPlayer(player, "bank.gui.buyacc.nomoney"));
                });
            }
            new IndexGui(this.player).open(this.player.toBukkitPlayer());
        });
    }
}
