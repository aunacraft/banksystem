package net.aunacraft.banksystem.gui;

import com.mojang.authlib.GameProfile;
import net.aunacraft.api.AunaAPI;
import net.aunacraft.api.gui.Gui;
import net.aunacraft.api.gui.GuiInventory;
import net.aunacraft.api.player.AunaPlayer;
import net.aunacraft.api.util.ItemBuilder;
import net.aunacraft.api.util.SkinFetcher;
import net.aunacraft.api.util.SkullBuilder;
import net.aunacraft.banksystem.BankSystem;
import net.aunacraft.banksystem.transaction.Transaction;
import net.aunacraft.banksystem.transaction.impl.DepositTransaction;
import net.aunacraft.banksystem.transaction.impl.WithdrawTransaction;
import net.aunacraft.banksystem.utils.LevelUtil;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.profile.PlayerProfile;


public class IndexGui extends Gui {

    private final AunaPlayer player;

    public IndexGui(AunaPlayer opener) {
        super(BankSystem.getInstance().getMessageService().getMessageForPlayer(opener.toBukkitPlayer(), "bank.gui.index.title"), 45);
        this.player = opener;
    }

    @Override
    public void initInventory(Player player, GuiInventory guiInventory) {
        guiInventory.fill(new ItemBuilder(Material.GRAY_STAINED_GLASS_PANE).setDisplayName(" ").build());
        guiInventory.setItem(0, new ItemBuilder(Material.YELLOW_STAINED_GLASS_PANE).setDisplayName(" ").build());
        guiInventory.setItem(8, new ItemBuilder(Material.YELLOW_STAINED_GLASS_PANE).setDisplayName(" ").build());
        guiInventory.setItem(36, new ItemBuilder(Material.YELLOW_STAINED_GLASS_PANE).setDisplayName(" ").build());
        guiInventory.setItem(44, new ItemBuilder(Material.YELLOW_STAINED_GLASS_PANE).setDisplayName(" ").build());

        AunaPlayer aunaPlayer = AunaAPI.getApi().getPlayer(player.getUniqueId());

        ItemStack headStack = new ItemStack(Material.PLAYER_HEAD);
        SkullMeta meta = (SkullMeta) headStack.getItemMeta();
        meta.setOwner(aunaPlayer.getName());
        meta.setDisplayName(BankSystem.getInstance().getMessageService().getMessageForPlayer(player,
                "bank.gui.index.head",
                aunaPlayer.getCachedBankCoins()));
        headStack.setItemMeta(meta);

        guiInventory.setItem(13, headStack);
        guiInventory.setItem(20, new ItemBuilder(Material.HOPPER).setDisplayName(BankSystem.getInstance().getMessageService().getMessageForPlayer(player, "bank.gui.index.hopper")).build(),
                inventoryClickEvent -> {
                    BankSystem.getInstance().getGeneralLevel().getLevel(aunaPlayer, level -> {
                        if(aunaPlayer.getCachedBankCoins() >= LevelUtil.maxAmountInBank(level)) {
                            aunaPlayer.sendMessage("bank.tooMuchInBankLowLevel", LevelUtil.maxAmountInBank(level));
                            return;
                        }
                        Transaction transaction = new DepositTransaction(this.player, 0);
                        BankSystem.getInstance().getTransactionManager().getPlayerTransactionCache().put(this.player, transaction);
                    });
                });
        guiInventory.setItem(24, new ItemBuilder(Material.DISPENSER).setDisplayName(BankSystem.getInstance().getMessageService().getMessageForPlayer(player, "bank.gui.index.dispenser")).build(),
                inventoryClickEvent -> {
                    Transaction transaction = new WithdrawTransaction(this.player, 0);
                    BankSystem.getInstance().getTransactionManager().getPlayerTransactionCache().put(this.player, transaction);
                });
        guiInventory.setItem(31, new ItemBuilder(Material.CHEST_MINECART).setDisplayName(BankSystem.getInstance().getMessageService().getMessageForPlayer(player, "bank.gui.index.settings")).build(),
                inventoryClickEvent -> {
                    new SettingsGui(AunaAPI.getApi().getPlayer(player.getUniqueId())).open(player);
                    player.playSound(player.getLocation(), Sound.BLOCK_NOTE_BLOCK_BIT, 1, 1);
                });
    }
}
