package net.aunacraft.banksystem.gui;

import net.aunacraft.api.AunaAPI;
import net.aunacraft.api.gui.Gui;
import net.aunacraft.api.gui.GuiInventory;
import net.aunacraft.api.player.AunaPlayer;
import net.aunacraft.api.util.ItemBuilder;
import net.aunacraft.api.util.SkullBuilder;
import net.aunacraft.banksystem.BankSystem;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;

import javax.sql.rowset.CachedRowSet;
import java.sql.SQLException;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;

public class SettingsGui extends Gui {

    private final AtomicInteger bankLevel;
    private final AtomicInteger tresorLevel;
    private final AunaPlayer aunaPlayer;

    public SettingsGui(AunaPlayer player) {
        super(BankSystem.getInstance().getMessageService().getMessageForPlayer(player.toBukkitPlayer(), "bank.gui.settings.title"), 45);
        bankLevel = new AtomicInteger();
        tresorLevel = new AtomicInteger();
        aunaPlayer = player;
    }

    public void setBankLevel(int bankLevel) {
        this.bankLevel.set(bankLevel);
    }

    public void setTresorLevel(int tresorLevel) {
        this.tresorLevel.set(tresorLevel);
    }

    private String getURL(String texture) {
        return "http://textures.minecraft.net/texture/" + texture;
    }

    @Override
    public void initInventory(Player player, GuiInventory guiInventory) {
        guiInventory.fill(new ItemBuilder(Material.GRAY_STAINED_GLASS_PANE).setDisplayName(" ").build());
        guiInventory.setItem(0, new ItemBuilder(Material.YELLOW_STAINED_GLASS_PANE).setDisplayName(" ").build());
        guiInventory.setItem(8, new ItemBuilder(Material.YELLOW_STAINED_GLASS_PANE).setDisplayName(" ").build());
        guiInventory.setItem(36, new ItemBuilder(SkullBuilder.getSkull(getURL("37aee9a75bf0df7897183015cca0b2a7d755c63388ff01752d5f4419fc645")))
                .setDisplayName(BankSystem.getInstance().getMessageService().getMessageForPlayer(player, "bank.gui.back"))
                .build(), inventoryClickEvent -> Bukkit.getScheduler().scheduleSyncDelayedTask(BankSystem.getInstance(),
                () -> new IndexGui(AunaAPI.getApi().getPlayer(player.getUniqueId())).open(player)));
        guiInventory.setItem(44, new ItemBuilder(Material.YELLOW_STAINED_GLASS_PANE).setDisplayName(" ").build());
        guiInventory.setItem(20, new ItemBuilder(Material.CHAIN).setDisplayName(" ").build());
        guiInventory.setItem(24, new ItemBuilder(Material.CHAIN).setDisplayName(" ").build());
        guiInventory.setItem(22, new ItemBuilder(SkullBuilder.getSkull(AunaAPI.getApi().getRealUUID(player))).setDisplayName(BankSystem.getInstance().getMessageService().getMessageForPlayer(player, "bank.gui.settings.head", player.getName())).build());

        BankSystem.getInstance().getDatabaseHandler().createBuilder("SELECT * FROM bank_generalBank WHERE uuid = ?")
                .addObjects(AunaAPI.getApi().getRealUUID(player).toString()).queryAsync(cachedRowSet -> {
                    try {
                        int aLevel = -1;
                        int bLevel = -1;
                        if (cachedRowSet.next()) {
                            aLevel = cachedRowSet.getInt("bankLevel");
                            bLevel = cachedRowSet.getInt("tresorLevel");
                        }
                        setBankLevel(aLevel);
                        setTresorLevel(bLevel);

                        final int bankLevelUpgradeCost = (bankLevel.get() == 1 ? 10000 : bankLevel.get() == 2 ? 25000 : bankLevel.get() == 3 ? 50000 : 100000);
                        final int tresorLevelUpgradeCost = (tresorLevel.get() == 0 ? 1000 : tresorLevel.get() == 1 ? 5000
                                : tresorLevel.get() == 2 ? 10000 : tresorLevel.get() == 3 ? 20000 : tresorLevel.get() == 4 ? 50000 : 100000);

                        guiInventory.setItem(11, new ItemBuilder((
                                aLevel == 1 ? Material.MINECART : aLevel == 2 ? Material.HOPPER_MINECART :
                                        aLevel == 3 ? Material.FURNACE_MINECART : aLevel == 4 ? Material.CHEST_MINECART : Material.BARRIER
                        )).setDisplayName(BankSystem.getInstance().getMessageService().getMessageForPlayer(player, "bank.gui.settings.level", aLevel)).build());
                        guiInventory.setItem(15, new ItemBuilder(Material.CHEST).setDisplayName(BankSystem.getInstance().getMessageService().getMessageForPlayer(player, "bank.gui.settings.tresor", bLevel)).build());

                        guiInventory.setItem(29, new ItemBuilder(Material.EMERALD).setDisplayName(BankSystem.getInstance().getMessageService().getMessageForPlayer(player, "bank.gui.settings.level.upgrade", (
                                        aLevel != 5 && aLevel > 0 ? "§e" + aLevel + "§8 -> §e"
                                                + (aLevel + 1) : aunaPlayer.getMessage("bank.gui.settings.highestlevel.gui"))))
                                .setLore(BankSystem.getInstance().getMessageService().getMessageForPlayer(player, "bank.gui.settings.level.upgrade.lore.1"),
                                        BankSystem.getInstance().getMessageService().getMessageForPlayer(player, "bank.gui.settings.level.upgrade.lore.2"),
                                        BankSystem.getInstance().getMessageService().getMessageForPlayer(player, "bank.gui.settings.level.upgrade.lore.3")).build(), inventoryClickEvent -> {
                            if (bankLevel.get() != 5 && bankLevel.get() > 0) {
                                final AunaPlayer aunaPlayer = AunaAPI.getApi().getPlayer(player.getUniqueId());
                                AunaAPI.getApi().getCoinService().buyTransaction(aunaPlayer.getUuid(), bankLevelUpgradeCost, able -> {
                                    if (able) {
                                        BankSystem.getInstance().getGeneralLevel().setLevel(aunaPlayer, bankLevel.get() + 1);
                                        player.sendMessage(BankSystem.getInstance().getMessageService().getMessageForPlayer(player, "bank.gui.settings.upgradelevel.success"));
                                        Bukkit.getScheduler().scheduleSyncDelayedTask(BankSystem.getInstance(), () -> new SettingsGui(aunaPlayer).open(player));
                                    } else
                                        player.sendMessage(BankSystem.getInstance().getMessageService().getMessageForPlayer(player, "bank.gui.buyacc.nomoney"));
                                });
                            } else
                                player.sendMessage(BankSystem.getInstance().getMessageService().getMessageForPlayer(player, "bank.gui.settings.highestlevel"));
                        });
                        guiInventory.setItem(33, new ItemBuilder(Material.EMERALD).setDisplayName(BankSystem.getInstance().getMessageService().getMessageForPlayer(player, "bank.gui.settings.tresor.upgrade", (
                                bLevel != 5 && bLevel > 0 ? "§e" + bLevel + "§8 -> §e" + (bLevel + 1) :
                                        bLevel == 0 ? "§cX §8-> §e1" : aunaPlayer.getMessage("bank.gui.settings.highestlevel.gui")
                        ))).setLore(BankSystem.getInstance().getMessageService().getMessageForPlayer(player, "bank.gui.settings.tresor.upgrade.lore.1"),
                                BankSystem.getInstance().getMessageService().getMessageForPlayer(player, "bank.gui.settings.tresor.upgrade.lore.2"),
                                BankSystem.getInstance().getMessageService().getMessageForPlayer(player, "bank.gui.settings.tresor.upgrade.lore.3")).build(), inventoryClickEvent -> {
                            if (tresorLevel.get() != 5 && tresorLevel.get() > -1) {
                                final AunaPlayer aunaPlayer = AunaAPI.getApi().getPlayer(player.getUniqueId());
                                AunaAPI.getApi().getCoinService().buyTransaction(aunaPlayer.getUuid(), tresorLevelUpgradeCost, able -> {
                                    if (able) {
                                        BankSystem.getInstance().getTresorLevel().setLevel(aunaPlayer, tresorLevel.get() + 1);
                                        player.sendMessage(BankSystem.getInstance().getMessageService().getMessageForPlayer(player, "bank.gui.settings.upgradelevel.success"));
                                        Bukkit.getScheduler().scheduleSyncDelayedTask(BankSystem.getInstance(), () -> new SettingsGui(aunaPlayer).open(player));
                                    } else
                                        player.sendMessage(BankSystem.getInstance().getMessageService().getMessageForPlayer(player, "bank.gui.buyacc.nomoney"));
                                });
                            } else
                                player.sendMessage(BankSystem.getInstance().getMessageService().getMessageForPlayer(player, "bank.gui.settings.highestlevel"));
                        });

                    } catch (SQLException throwables) {
                        throwables.printStackTrace();
                    }
                });
    }
}
