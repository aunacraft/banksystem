package net.aunacraft.banksystem.gui;

import net.aunacraft.api.AunaAPI;
import net.aunacraft.api.gui.Gui;
import net.aunacraft.api.gui.GuiInventory;
import net.aunacraft.api.util.ItemBuilder;
import net.aunacraft.api.util.SkullBuilder;
import net.aunacraft.banksystem.BankSystem;
import net.aunacraft.banksystem.transaction.Transaction;
import net.aunacraft.banksystem.transaction.TransactionChatSession;
import net.aunacraft.banksystem.transaction.impl.DepositTransaction;
import net.aunacraft.banksystem.utils.LevelUtil;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;

public class TransactionGui extends Gui {

    private final Transaction transaction;

    public TransactionGui(Transaction transaction) {
        super(BankSystem.getInstance().getMessageService().getMessageForPlayer(transaction.getOwner().toBukkitPlayer(), "bank.gui.transaction.title." + (
                transaction.isDeposit() ? "deposit" : "withdraw"
        )), 27);
        this.transaction = transaction;
    }

    @Override
    public void initInventory(Player player, GuiInventory guiInventory) {
        guiInventory.fill(new ItemBuilder(Material.GRAY_STAINED_GLASS_PANE).setDisplayName(" ").build());
        guiInventory.setItem(0, new ItemBuilder(Material.YELLOW_STAINED_GLASS_PANE).setDisplayName(" ").build());
        guiInventory.setItem(8, new ItemBuilder(Material.YELLOW_STAINED_GLASS_PANE).setDisplayName(" ").build());
        guiInventory.setItem(26, new ItemBuilder(Material.YELLOW_STAINED_GLASS_PANE).setDisplayName(" ").build());
        guiInventory.setItem(18, new ItemBuilder(SkullBuilder.getSkull("http://textures.minecraft.net/texture/cdc9e4dcfa4221a1fadc1b5b2b11d8beeb57879af1c42362142bae1edd5"))
                        .setDisplayName(transaction.getOwner().getMessage("bank.gui.back")).build(),
                inventoryClickEvent -> new IndexGui(AunaAPI.getApi().getPlayer(player.getUniqueId())).open(player));
        guiInventory.setItem(11, new ItemBuilder(Material.HOPPER_MINECART).setDisplayName("§e50%").build(), inventoryClickEvent -> {
            if (transaction.isDeposit()) {
                AunaAPI.getApi().getCoinService().getCoins(transaction.getOwner().getUuid(), coins -> {
                    BankSystem.getInstance().getGeneralLevel().getLevel(transaction.getOwner(), level -> {
                        if(transaction.getOwner().getCachedBankCoins() + coins / 2 >= LevelUtil.maxAmountInBank(level)) {
                            transaction.getOwner().sendMessage("bank.tooMuchInBankLowLevel", LevelUtil.maxAmountInBank(level));
                            return;
                        }
                        transaction.setAmount(coins / 2);
                        transaction.next();
                        Bukkit.getScheduler().scheduleSyncDelayedTask(BankSystem.getInstance(), () ->
                                new ConfirmGui(this.transaction.getOwner(), this.transaction).open(this.transaction.getOwner().toBukkitPlayer()));
                    });
                });
            } else {
                AunaAPI.getApi().getCoinService().getBankCoins(transaction.getOwner().getUuid(), coins -> {
                    transaction.setAmount(coins / 2);
                    transaction.next();
                    Bukkit.getScheduler().scheduleSyncDelayedTask(BankSystem.getInstance(), () ->
                            new ConfirmGui(this.transaction.getOwner(), this.transaction).open(this.transaction.getOwner().toBukkitPlayer()));
                });
            }
        });
        guiInventory.setItem(15, new ItemBuilder(Material.OAK_SIGN).setDisplayName(BankSystem.getInstance().getMessageService().getMessageForPlayer(player, "bank.gui.transaction.customamount")).build(),
                inventoryClickEvent -> {
                    TransactionChatSession session = new TransactionChatSession(this.transaction).start();
                    session.getTransaction().getOwner().toBukkitPlayer().sendMessage(BankSystem.getInstance().getMessageService().getMessageForPlayer(player, "bank.gui.transactiongui.custom.chat"));
                    Bukkit.getScheduler().scheduleSyncDelayedTask(BankSystem.getInstance(), player::closeInventory);
                });
        guiInventory.setItem(13, new ItemBuilder(Material.CHEST_MINECART).setDisplayName("§e100%").build(), inventoryClickEvent -> {
            if (transaction.isDeposit()) {
                AunaAPI.getApi().getCoinService().getCoins(transaction.getOwner().getUuid(), coins -> {
                    BankSystem.getInstance().getGeneralLevel().getLevel(transaction.getOwner(), level -> {
                        if(transaction.getOwner().getCachedBankCoins() + coins >= LevelUtil.maxAmountInBank(level)) {
                            transaction.getOwner().sendMessage("bank.tooMuchInBankLowLevel", LevelUtil.maxAmountInBank(level));
                            return;
                        }
                        transaction.setAmount(coins);
                        transaction.next();
                        Bukkit.getScheduler().scheduleSyncDelayedTask(BankSystem.getInstance(), () ->
                                new ConfirmGui(this.transaction.getOwner(), this.transaction).open(this.transaction.getOwner().toBukkitPlayer()));
                    });
                });
            } else {
                AunaAPI.getApi().getCoinService().getBankCoins(transaction.getOwner().getUuid(), coins -> {
                    transaction.setAmount(coins);
                    transaction.next();
                    Bukkit.getScheduler().scheduleSyncDelayedTask(BankSystem.getInstance(), () ->
                            new ConfirmGui(this.transaction.getOwner(), this.transaction).open(this.transaction.getOwner().toBukkitPlayer()));
                });
            }
        });
    }

    @Override
    public void onClick(Player player, InventoryClickEvent event) {
        transaction.cancel();
        super.onClick(player, event);
    }
}
