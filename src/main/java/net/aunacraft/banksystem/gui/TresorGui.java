package net.aunacraft.banksystem.gui;

import net.aunacraft.api.AunaAPI;
import net.aunacraft.api.gui.Gui;
import net.aunacraft.api.gui.GuiInventory;
import net.aunacraft.api.player.AunaPlayer;
import net.aunacraft.api.util.ItemBuilder;
import net.aunacraft.api.util.SkullBuilder;
import net.aunacraft.banksystem.BankSystem;
import net.aunacraft.banksystem.tresor.item.ItemTresorHandler;
import net.aunacraft.banksystem.tresor.item.TresorItem;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;

public class TresorGui extends Gui {

    private final List<ItemStack> blockedItems;

    public TresorGui(AunaPlayer player, int level) {
        super(BankSystem.getInstance().getMessageService().getMessageForPlayer(player.toBukkitPlayer(), "bank.gui.tresor.inventory.title"),
                9 * level + 9);
        this.blockedItems = new ArrayList<>();
    }

    @Override
    public void initInventory(Player player, GuiInventory guiInventory) {
        guiInventory.setCancelClick(false);
        AunaPlayer aunaPlayer = AunaAPI.getApi().getPlayer(player.getUniqueId());
        for (int i = guiInventory.getSize() - 9; i < guiInventory.getSize(); i++)
            guiInventory.setItem(i, new ItemBuilder(Material.RED_STAINED_GLASS_PANE).setDisplayName(" ").build(), inventoryClickEvent -> {
                inventoryClickEvent.setCancelled(true);
            });

        /*guiInventory.setItem(guiInventory.getSize() - 9, new ItemBuilder(SkullBuilder.getSkull(getURL("37aee9a75bf0df7897183015cca0b2a7d755c63388ff01752d5f4419fc645")))
                .setDisplayName(BankSystem.getInstance().getMessageService().getMessageForPlayer(player, "bank.gui.back"))
                .build(), inventoryClickEvent -> {
            inventoryClickEvent.setCancelled(true);
            /*if (!Arrays.equals(inventoryClickEvent.getClickedInventory().getContents(), this.inventory.getContents()))
                new ItemTresorHandler().setItems(AunaAPI.getApi().getRealUUID(player).toString(),
                        new ItemTresorHandler().getItemList(inventoryClickEvent.getClickedInventory(), this.blockedItems));
            Bukkit.getScheduler().scheduleSyncDelayedTask(BankSystem.getInstance(), player::closeInventory);
        });*/
        BankSystem.getInstance().getItemTresorHandler().getItems(aunaPlayer.getUuid().toString(), tresorItems -> {
            if(tresorItems != null){
                for (TresorItem items : tresorItems) {
                    guiInventory.setItem(items.getSlot(), items.getContent());
                }
            }
        });
    }

    private String getURL(String texture) {
        return "http://textures.minecraft.net/texture/" + texture;
    }

    @Override
    public void onClose(Player player) {
        BankSystem.getInstance().getItemTresorHandler().setItems(AunaAPI.getApi().getRealUUID(player).toString(),
                BankSystem.getInstance().getItemTresorHandler().getItemList(player.getOpenInventory().getTopInventory(), this.blockedItems));
        super.onClose(player);
    }

}
