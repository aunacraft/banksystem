package net.aunacraft.banksystem.level;

import net.aunacraft.api.player.AunaPlayer;
import net.aunacraft.banksystem.BankSystem;
import net.aunacraft.banksystem.level.impl.GeneralLevelHandler;

import javax.sql.rowset.CachedRowSet;
import java.sql.ResultSet;
import java.util.function.Consumer;

public abstract class LevelHandler {

    public abstract String getKeyPrefix();

    public void getLevel(final AunaPlayer craftingEnginePlayer, final Consumer<Integer> callback) {
        BankSystem.getInstance().getDatabaseHandler().createBuilder("SELECT * FROM bank_generalBank WHERE uuid = ?")
                .addObjects(craftingEnginePlayer.getUuid().toString()).queryAsync(cachedRowSet -> {
            try {
                if (cachedRowSet.next()) {
                    callback.accept(cachedRowSet.getInt((
                            isGeneralLevel() ? "bankLevel" : "tresorLevel"
                    )));
                }
            } catch (Exception exception) {
                exception.printStackTrace();
            }
        });
    }

    public int getLevelSync(final AunaPlayer aunaPlayer) {
        ResultSet rs = BankSystem.getInstance().getDatabaseHandler().createBuilder("SELECT * FROM bank_generalBank WHERE uuid = ?").addObjects(aunaPlayer.getUuid().toString()).querySync();
        try {
            if (rs.next()) {
                return rs.getInt((
                        isGeneralLevel() ? "bankLevel" : "tresorLevel"
                ));
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }
        return -1;
    }

    public boolean isGeneralLevel() {
        return (this instanceof GeneralLevelHandler);
    }

    public boolean hasLevel(final AunaPlayer craftingEnginePlayer) {
        return (
                !craftingEnginePlayer.getPlayerMeta().isNull(getKeyPrefix())
        );
    }

    public LevelHandler setLevel(final AunaPlayer craftingEnginePlayer, final int level) {
        BankSystem.getInstance().getDatabaseHandler()
                .createBuilder("UPDATE bank_generalBank SET " + (this instanceof GeneralLevelHandler ? "bankLevel" : "tresorLevel") + " = ? WHERE uuid = ?")
                .addObjects(level, craftingEnginePlayer.getUuid().toString()).updateAsync();
        return this;
    }

}
