package net.aunacraft.banksystem.level.impl;

import net.aunacraft.banksystem.level.LevelHandler;

public class GeneralLevelHandler extends LevelHandler {
    @Override
    public String getKeyPrefix() {
        return "level.general";
    }
}
