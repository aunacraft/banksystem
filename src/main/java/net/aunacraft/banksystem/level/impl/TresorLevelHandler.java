package net.aunacraft.banksystem.level.impl;

import net.aunacraft.banksystem.level.LevelHandler;

public class TresorLevelHandler extends LevelHandler {
    @Override
    public String getKeyPrefix() {
        return "level.tresor";
    }
}
