package net.aunacraft.banksystem.listeners;

import net.aunacraft.api.AunaAPI;
import net.aunacraft.api.event.impl.PlayerRegisterEvent;
import net.aunacraft.api.player.AunaPlayer;
import net.aunacraft.banksystem.BankSystem;
import org.bukkit.event.Listener;

import javax.sql.rowset.CachedRowSet;
import java.util.function.Consumer;

public class JoinListener {

    public JoinListener() {
        AunaAPI.getApi().registerEventListener(PlayerRegisterEvent.class, this::handlePlayerJoin);
    }

    public void handlePlayerJoin(PlayerRegisterEvent event) {
        AunaPlayer player = event.getPlayer();
        BankSystem.getInstance().getDatabaseHandler().createBuilder("SELECT * FROM bank_generalBank WHERE uuid = ?").addObjects(player.getUuid().toString()).queryAsync(new Consumer<CachedRowSet>() {
            @Override
            public void accept(CachedRowSet cachedRowSet) {
                try {
                    if (!cachedRowSet.next()) {
                        BankSystem.getInstance().getDatabaseHandler().createBuilder("INSERT INTO bank_generalBank(uuid, bankLevel, tresorLevel) VALUES (?,?,?)").addObjects(player.getUuid().toString(), 0, 0).updateSync();
                    }
                } catch (Exception e) {
                    player.toBukkitPlayer().kickPlayer("§cUnable to set bank database. We would appreciate to report this.");
                    e.printStackTrace();
                }
            }
        });
    }

}
