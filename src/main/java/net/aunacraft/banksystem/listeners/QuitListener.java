package net.aunacraft.banksystem.listeners;

import net.aunacraft.api.AunaAPI;
import net.aunacraft.api.event.impl.PlayerUnregisterEvent;
import net.aunacraft.banksystem.utils.Interests;
import org.bukkit.event.Listener;

public class QuitListener {

    public QuitListener() {
        AunaAPI.getApi().registerEventListener(PlayerUnregisterEvent.class, this::handlePlayerQuit);
    }

    public void handlePlayerQuit(PlayerUnregisterEvent event) {
        //check if has konto
        event.getPlayer().getPlayerMeta().set(Interests.META_NAME, System.currentTimeMillis());
        event.getPlayer().updateMeta();
    }

}
