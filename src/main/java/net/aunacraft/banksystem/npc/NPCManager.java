package net.aunacraft.banksystem.npc;

import com.google.common.collect.Lists;
import net.aunacraft.api.AunaAPI;
import net.aunacraft.api.event.impl.LocationSetEvent;
import net.aunacraft.banksystem.BankSystem;
import net.aunacraft.banksystem.npc.impl.BankServiceNPC;
import org.bukkit.Bukkit;

import java.util.List;

public class NPCManager {

    private final List<NPCService> npcServices;

    public NPCManager(List<NPCService> npcServices) {
        this.npcServices = npcServices;

        Bukkit.getScheduler().runTaskLater(BankSystem.getInstance(), () -> {
            npcServices.forEach(npcService -> BankSystem.getInstance().getSpigotLocationProvider().hasLocationSaved(npcService.id(), isThere -> {
                if(isThere)
                    npcService.init(BankSystem.getInstance().getSpigotLocationProvider().getLocationSync(npcService.id()));
                else
                    Bukkit.getLogger().info("Found unset NPC-Location '" + npcService.id() + "'. Please set it with /setlocation " + BankSystem.getInstance().getDescription().getName() + " " + npcService.id());
            }));
        }, 20);

        AunaAPI.getApi().registerEventListener(LocationSetEvent.class, locationSetEvent -> {
            if(!locationSetEvent.getLocationProvider().equals(BankSystem.getInstance().getSpigotLocationProvider())) return;
            npcServices.forEach(npcService -> {
                if(npcService.id().equals(locationSetEvent.getKey()))
                    npcService.init(locationSetEvent.getLocationProvider().getLocationSync(locationSetEvent.getKey()));
            });
        });
    }

    public List<NPCService> getNpcServices() {
        return npcServices;
    }
}
