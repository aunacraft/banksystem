package net.aunacraft.banksystem.npc;

import net.aunacraft.api.bukkit.npc.event.NPCInteractEvent;
import org.bukkit.Location;

public interface NPCService {

    void init(Location location);
    void handleInteract(NPCInteractEvent event);
    String id();
}
