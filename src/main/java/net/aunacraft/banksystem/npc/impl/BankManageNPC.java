package net.aunacraft.banksystem.npc.impl;

import net.aunacraft.api.AunaAPI;
import net.aunacraft.api.bukkit.npc.builder.NPCBuilder;
import net.aunacraft.api.bukkit.npc.event.NPCInteractEvent;
import net.aunacraft.api.player.AunaPlayer;
import net.aunacraft.api.util.SkinFetcher;
import net.aunacraft.banksystem.BankSystem;
import net.aunacraft.banksystem.gui.BuyBankAccountGui;
import net.aunacraft.banksystem.gui.IndexGui;
import net.aunacraft.banksystem.npc.NPCService;
import org.bukkit.Bukkit;
import org.bukkit.Location;

public class BankManageNPC implements NPCService {
    @Override
    public void init(Location location) {
        new NPCBuilder(BankSystem.getInstance())
                .setLocation(location)
                .setInteractListener(this::handleInteract)
                .setLookAtPlayer(true)
                .setPunchPlayers(true)
                .setPunchCallback(aunaPlayer -> aunaPlayer.sendMessage("bank.npc.security.warn", aunaPlayer.getName()))
                .setNameKey("bank.npc.manager.display")
                .setSkin(new SkinFetcher.SkinObject("eyJ0aW1lc3RhbXAiOjE1ODEyNTg5NzQzNzEsInByb2ZpbGVJZCI6IjczODJkZGZiZTQ4NTQ1NWM4MjVmOTAwZjg4ZmQzMmY" +
                        "4IiwicHJvZmlsZU5hbWUiOiJZYU9PUCIsInNpZ25hdHVyZVJlcXVpcmVkIjp0cnVlLCJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY" +
                        "3JhZnQubmV0L3RleHR1cmUvZjhkNGQwZjdmYmIyY2MwYWNjMDI4MThiZDhkZjA1NTk0NjU3Mzg4NjVmZDg2M2FjMTc1M2ViNGVmZDJhMjgxOSJ9fX0=",
                        "DEucePZDjL5QUaq0Dx9s5Wn1HHxuyREZpy4Lt1h17TMGsz2uYxfwR9o/oUNhCxQoq5sqQ7JlT5p/DdpbaK0jALAeUXxhWBgLOzII0jIx1EPq" +
                                "VhgSSJmvyx+YZfj5lF6eAhN1WgaPPDSzf5klJ8/5lf9JzZJ64uMLWY5wf2rtCVejHTeZMj+iujTwURPpWPcLRC1FziYSvwyHQAi7U8ZfZoBv2g" +
                                "d/CObXFjzURkFrQA1Py4D3NI7kT1P1sN+HaMdPOHMS/QtdurTP3N5ZCkZnyQH0aEd6BUj9WzsRrMlIdj6KjWrkzVgbXItuUIDoSfGSFVoUJzW" +
                                "gFR+edWmaurWhtO/Z4rIkc57b4H6uPvwZeMEfuEH+105aIVZer99d9TRqU2VGTooangpM6ycsndSweJxOrKblVVgOaoqaj/EEu+UAKZePsWBZY" +
                                "l2FQ3qN/lOf/Ux9vE2kUfZNyOiSpiCeid3CDqzkiliOizSyXCSsOYSVVBgwyeEOD2RA30PZ4idxbKTK6aGczkuJYfmITLpEVcUr8C8xJHGrCc3" +
                                "LILii4eVC9gfbpIQ+xpAOfCcjcZolSYI+Oc2YvO2Q40VTkMf746syDoTjWvffEfaO44Znx2ToMCcw5tB0aaT1u/66VugpcXqXHnSab/5ZE+bgha3" +
                                "DdwE7B9eLfOStcQJQwxVYL9E="))
                .build();
    }

    @Override
    public void handleInteract(NPCInteractEvent event) {
        AunaPlayer player = AunaAPI.getApi().getPlayer(event.getPlayer().getUniqueId());
        BankSystem.getInstance().getGeneralLevel().getLevel(player, integer -> Bukkit.getScheduler().scheduleSyncDelayedTask(BankSystem.getInstance(), () -> {
            if(integer == 0) new BuyBankAccountGui(player).open(event.getPlayer());
            else new IndexGui(player).open(event.getPlayer());
        }));
    }

    @Override
    public String id() {
        return "bank_managenpc";
    }
}
