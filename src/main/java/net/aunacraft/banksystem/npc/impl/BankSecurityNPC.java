package net.aunacraft.banksystem.npc.impl;

import net.aunacraft.api.bukkit.npc.builder.NPCBuilder;
import net.aunacraft.api.bukkit.npc.event.NPCInteractEvent;
import net.aunacraft.api.util.SkinFetcher;
import net.aunacraft.banksystem.BankSystem;
import net.aunacraft.banksystem.npc.NPCService;
import net.aunacraft.banksystem.utils.MathUtil;
import org.bukkit.Location;

public class BankSecurityNPC implements NPCService {
    @Override
    public void init(Location location) {
        new NPCBuilder(BankSystem.getInstance())
                .setLocation(location)
                .setInteractListener(this::handleInteract)
                .setLookAtPlayer(true)
                .setPunchPlayers(true)
                .setPunchCallback(aunaPlayer -> {
                    if(MathUtil.getRandom(1, 10) > 8) aunaPlayer.sendMessage("bank.npc.security.kickMsg", aunaPlayer.getName());
                })
                .setNameKey("bank.npc.security.display")
                .setSkin(new SkinFetcher.SkinObject("ewogICJ0aW1lc3RhbXAiIDogMTYyMDA1MDQ1MzE2OSwKICAicHJvZmlsZUlkIiA6ICJhNzdkNmQ2YmFjOWE0NzY3YTFhNzU" +
                        "1NjYxOTllYmY5MiIsCiAgInByb2ZpbGVOYW1lIiA6ICIwOEJFRDUiLAogICJzaWduYXR1cmVSZXF1aXJlZCIgOiB0cnVlLAogICJ0ZXh0dXJlcyIgOiB7CiAgICAiU0tJT" +
                        "iIgOiB7CiAgICAgICJ1cmwiIDogImh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvMmM1OWFjNjMzODExMjM4NDM3MzljNDk1OWE0YzdlOGU1ZDkzZmU" +
                        "5ODZmOGJhYmI4OTFkYjhiY2RhNDQ4MzUwMCIKICAgIH0KICB9Cn0=",
                        "UD6JzBhi5tYraqRktaDoqFGFPpRfIydUkmBsCsfOpMN39gdqyIYoIKNhQCPKJtXuY/nH+NlVvgZhfBmSdzGhyzXWYo0QJ0QW2WsH1OeoeVg/Zfpivv/RUS2rrht" +
                                "xWv6uPaaPqAGnaAleJREE27xFEXvoE2qspEwT22h8tWOlyTd+ay03SUK64ctzJPSLBh3UCrfr38S5g0xOiJiMm7gRlIevMXmiVb4ok0NIDXMruTUJtHLq1bhEynK" +
                                "WcpnY8ydkSWHQhIOpfwT64tZ5fKjHKZ0ZrtY+DNr+x8JDIqHwRwsNvqP8I2BSEtJZjJ0Yr45zPVZAP24a9MZxoVDsOgV4HqBAZTuqYNG7HlU5JPxNJFqDllwx9RI" +
                                "lL2eYaMrqUw9hiv9syZdkds+rcWIDhhp5M4ZKoZWqIH04R3U+Vbuj90baHq8JOVjchakxUDEPZhhIxDVBg3QwkmdSDuOnwTuOCcwWcCV9870mKWms8TxU8CfU5Kf" +
                                "+lZTHjxgCl4GthgccZ+rUG1SBXZgjWSInRXEJH4SDCeyODajV5vOfnjsU2lwV14nEKBkZL2I81+D8vHAqQQjZ4IahoK1C65L/SLnjgVXrCTLNQywZQGeOcUaOf82t" +
                                "Khgo7Bm40GI3yMqFhB/jQlPTrOtWE3aglsriyO/rOWKTlMuDLInGHGW65Ehcdyo="))
                .build(); // https://mineskin.org/253a717eccd24299ae0ba7614fdd6697
    }

    @Override
    public void handleInteract(NPCInteractEvent event) {

    }

    @Override
    public String id() {
        return "bank_securitynpc";
    }
}
