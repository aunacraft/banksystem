package net.aunacraft.banksystem.npc.impl;

import net.aunacraft.api.AunaAPI;
import net.aunacraft.api.bukkit.npc.base.NPC;
import net.aunacraft.api.bukkit.npc.base.NPCBase;
import net.aunacraft.api.bukkit.npc.builder.NPCBuilder;
import net.aunacraft.api.bukkit.npc.event.NPCInteractEvent;
import net.aunacraft.api.player.AunaPlayer;
import net.aunacraft.api.util.SkinFetcher;
import net.aunacraft.banksystem.BankSystem;
import net.aunacraft.banksystem.npc.NPCService;
import net.aunacraft.banksystem.utils.Interests;
import net.aunacraft.banksystem.utils.MathUtil;
import org.bukkit.Location;

public class BankServiceNPC implements NPCService {
    @Override
    public void init(Location location) {
        new NPCBuilder(BankSystem.getInstance())
                .setLocation(location)
                .setInteractListener(this::handleInteract)
                .setLookAtPlayer(true)
                .setPunchPlayers(true)
                .setPunchCallback(aunaPlayer -> aunaPlayer.sendMessage("bank.npc.security.warn", aunaPlayer.getName()))
                .setNameKey("bank.npc.serviceDisplay")
                .setSkin(new SkinFetcher.SkinObject("ewogICJ0aW1lc3RhbXAiIDogMTY0NTkwMzY3OTc4OSwKICAicHJvZmlsZUlkIiA6ICI0N2RhZjU4YmJkNTM0OWU2YT" +
                        "I1MWYzMDZkYzhlYjc2ZiIsCiAgInByb2ZpbGVOYW1lIiA6ICJZeXJvIiwKICAic2lnbmF0dXJlUmVxdWlyZWQiIDogdHJ1ZSwKICAidGV4dHVyZXMiIDogewogICA" +
                        "gIlNLSU4iIDogewogICAgICAidXJsIiA6ICJodHRwOi8vdGV4dHVyZXMubWluZWNyYWZ0Lm5ldC90ZXh0dXJlLzdkMTRjMGZlOGExN2QzNWYzMGIzNGQ5OTVmNTQ4Z" +
                        "DJkZTIyNjUxMTBiMGNlYTQ4MThkODMxYjcwNTFiZTdlZGYiLAogICAgICAibWV0YWRhdGEiIDogewogICAgICAgICJtb2RlbCIgOiAic2xpbSIKICAgICAgfQogICA" +
                        "gfQogIH0KfQ==", "yV/b5dl6c8LtQc9S7DluK4cMqqAycrxMS71y5pS7OCc28t1BbiltbMRnCHOIRHqnhroB+hyAOJYi4VQmLckEJwFChLrfDBdzgueCm" +
                        "2NI7/KUbEPpDKwmpPCe77evENbGBOultDbupi76HjTmxNBJhHKL/Cy2JcdSSq6PGwELHukhwPLLzC/3yJgNt8tboSl8ou/yhnWKSyoSF3aN/2mFWvXWWTTCqrtpz8DG" +
                        "xew7UPSWqNjhu+/bgLhOtTaDCMqK0aQ7iNTdJmpsuaRZ1Qx24coybK3AcNIisJOurQqhm/673C9MU9i1wKx/b9rTG6mE/kgEZZsH1U+MYMDIw4xXBSubLjctAbDk5uY" +
                        "KY+wEzyHbOMCZ7on/J/+X7nBUhiPRgqKYJX6rL51CEGezJCYvf1ep5E+Cf774dQ3pmTih39RuA1NTQRgCTUtoBuTptYXTRHZRPE/0QuhLWlNz/Tq5xF4s8qKUTrI2dZs" +
                        "i4BcL65ZDUCLjX8vVMUqJc1TozmVlSQjfCl/bYBONW9L4dOKYWCQ6QsQNFBoOiLdSA6vjSNfCuRjM8v7GHBXcEgjvN/hcLY8fTDuzZA6C/jOLlCBXoBHJ0ZTpgDHi1va" +
                        "G3+obQpblJgl9FlQdStgi5Z8xx4jWA0AET8nAioWpNIgMhFJ2h76nd8Gi48ikcmMO1EUgThk="))
                .build();
    }

    @Override
    public void handleInteract(NPCInteractEvent event) {
        AunaPlayer aunaPlayer = AunaAPI.getApi().getPlayer(event.getPlayer().getUniqueId());
        if((aunaPlayer.getProperty("last_service_interact") != null && System.currentTimeMillis() - ((Long) aunaPlayer.getProperty("last_service_interact")) < 5*60*1000)
        || aunaPlayer.getProperty("last_service_interact") == null){
            aunaPlayer.sendMessage("bank.npc.service.serviceMessage",
                    "Klaus",
                    aunaPlayer.getName(),
                    Interests.addInterests(aunaPlayer.getCachedBankCoins()),
                    Interests.addInterests(aunaPlayer.getCachedBankCoins())*7,
                    aunaPlayer.getCachedBankCoins(),
                    MathUtil.percent(aunaPlayer.getCachedBankCoins(), aunaPlayer.getCachedBankCoins() + aunaPlayer.getCachedCoins()));
            aunaPlayer.setProperty("last_service_interact", System.currentTimeMillis());
        }else
            aunaPlayer.sendMessage("bank.npc.service.delayMessage", "Klaus", aunaPlayer.getName());
    }

    @Override
    public String id() {
        return "bank_servicenpc";
    }

}
