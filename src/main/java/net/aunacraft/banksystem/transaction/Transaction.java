package net.aunacraft.banksystem.transaction;

import net.aunacraft.api.player.AunaPlayer;
import net.aunacraft.banksystem.gui.TransactionGui;
import net.aunacraft.banksystem.transaction.impl.DepositTransaction;
import net.aunacraft.banksystem.transaction.impl.WithdrawTransaction;

public abstract class Transaction {

    private double amount;
    private final AunaPlayer player;
    private TransactionState currentState;

    /**
     * Inits Trancaction Class and opens TransactionGui for player
     *
     * @param player
     * @param amount
     */
    public Transaction(AunaPlayer player, int amount) {
        this.amount = amount;
        this.player = player;
        this.currentState = TransactionState.SELECTING;
        new TransactionGui(this).open(this.player.toBukkitPlayer());
    }

    public AunaPlayer getOwner() {
        return this.player;
    }

    public TransactionState getCurrentState() {
        return currentState;
    }

    public void setCurrentState(TransactionState currentState) {
        this.currentState = currentState;
    }

    public void cancel() {
        this.currentState = TransactionState.CANCELLED;
    }

    public boolean isCancelled() {
        return currentState == TransactionState.CANCELLED;
    }

    public boolean isEnded() {
        return this.currentState == TransactionState.ENDED;
    }

    /**
     * Sets the Currentstate to the next one
     */
    public void next() {
        if (this.currentState == TransactionState.SELECTING) {
            this.currentState = TransactionState.CONFIRMING;
        } else if (this.currentState == TransactionState.CONFIRMING) {
            this.currentState = TransactionState.ENDED;
        }
    }

    public boolean isDeposit() {
        return (this instanceof DepositTransaction);
    }

    public boolean isWithdraw() {
        return (this instanceof WithdrawTransaction);
    }

    public String getConfirmString() {
        return "§e" + this.amount + "§8€";
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public enum TransactionState {
        SELECTING, CONFIRMING, ENDED, CANCELLED
    }
}
