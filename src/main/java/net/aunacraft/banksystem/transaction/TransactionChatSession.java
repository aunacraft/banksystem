package net.aunacraft.banksystem.transaction;

import net.aunacraft.banksystem.BankSystem;
import net.aunacraft.banksystem.gui.ConfirmGui;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

public class TransactionChatSession implements Listener {

    private final Transaction transaction;

    public TransactionChatSession(Transaction transaction) {
        this.transaction = transaction;
    }

    public Transaction getTransaction() {
        return transaction;
    }

    public TransactionChatSession start() {
        Bukkit.getPluginManager().registerEvents(this, BankSystem.getInstance());
        return this;
    }

    public TransactionChatSession stop() {
        HandlerList.unregisterAll(this);
        return this;
    }

    /**
     * Handles the Chat Input for the Session
     */
    @EventHandler(priority = EventPriority.LOWEST)
    public void onChat(AsyncPlayerChatEvent event) {
        if (!(event.getPlayer().equals(transaction.getOwner().toBukkitPlayer()))) return;
        event.setCancelled(true);
        String message = event.getMessage();
        if (this.isValidInt(message)) {
            Bukkit.getScheduler().scheduleSyncDelayedTask(BankSystem.getInstance(), () -> {
                if (this.transaction.isDeposit()) {
                    if (this.transaction.getOwner().getCachedCoins() >= Integer.parseInt(message)) {
                        this.stop();
                        this.transaction.setAmount(Integer.parseInt(message));
                        this.transaction.next();
                        new ConfirmGui(this.transaction.getOwner(), this.transaction).open(this.transaction.getOwner().toBukkitPlayer());
                    } else {
                        event.getPlayer().sendMessage(BankSystem.getInstance().getMessageService().getMessageForPlayer(event.getPlayer(), "bank.transaction.customamount.notenoughmoney"));
                    }
                } else {
                    if (this.transaction.getOwner().getCachedBankCoins() >= Integer.parseInt(message)) {
                        this.stop();
                        this.transaction.setAmount(Integer.parseInt(message));
                        this.transaction.next();
                        new ConfirmGui(this.transaction.getOwner(), this.transaction).open(this.transaction.getOwner().toBukkitPlayer());
                    } else {
                        event.getPlayer().sendMessage(BankSystem.getInstance().getMessageService().getMessageForPlayer(event.getPlayer(), "bank.transaction.customamount.notenoughmoney"));
                    }
                }
            });
        } else {
            event.getPlayer().sendMessage(BankSystem.getInstance().getMessageService().getMessageForPlayer(event.getPlayer(), "bank.transaction.customamount.numberexception"));
        }
    }

    private boolean isValidInt(String in) {
        try {
            int i = Integer.parseInt(in);
            return (i >= 100);
        } catch (NumberFormatException e) {
            return false;
        }
    }
}
