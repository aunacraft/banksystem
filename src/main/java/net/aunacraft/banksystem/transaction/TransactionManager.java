package net.aunacraft.banksystem.transaction;

import net.aunacraft.api.player.AunaPlayer;
import org.bukkit.plugin.Plugin;

import java.util.HashMap;

public class TransactionManager {

    private final Plugin plugin;
    private final HashMap<AunaPlayer, Transaction> playerTransactionCache;

    public TransactionManager(Plugin plugin) {
        this.plugin = plugin;
        this.playerTransactionCache = new HashMap<>();
    }

    public HashMap<AunaPlayer, Transaction> getPlayerTransactionCache() {
        return playerTransactionCache;
    }
}
