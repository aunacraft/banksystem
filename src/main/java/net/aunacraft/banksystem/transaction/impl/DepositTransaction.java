package net.aunacraft.banksystem.transaction.impl;

import net.aunacraft.api.player.AunaPlayer;
import net.aunacraft.banksystem.transaction.Transaction;

public class DepositTransaction extends Transaction {
    public DepositTransaction(final AunaPlayer player, final int amount) {
        super(player, amount);
    }
}
