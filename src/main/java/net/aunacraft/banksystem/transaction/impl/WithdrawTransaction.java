package net.aunacraft.banksystem.transaction.impl;

import net.aunacraft.api.player.AunaPlayer;
import net.aunacraft.banksystem.transaction.Transaction;

public class WithdrawTransaction extends Transaction {
    public WithdrawTransaction(final AunaPlayer player, final int amount) {
        super(player, amount);
    }
}
