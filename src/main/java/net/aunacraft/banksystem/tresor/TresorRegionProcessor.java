package net.aunacraft.banksystem.tresor;

import net.aunacraft.api.AunaAPI;
import net.aunacraft.api.bukkit.util.LocationUtil;
import net.aunacraft.api.player.AunaPlayer;
import net.aunacraft.banksystem.BankSystem;
import net.aunacraft.banksystem.gui.TresorGui;
import org.bukkit.Bukkit;
import org.bukkit.Location;

public class TresorRegionProcessor {

    private Location location1, location2,
                    teleportBackLocation;

    public TresorRegionProcessor(){
        Bukkit.getScheduler().runTaskLater(BankSystem.getInstance(), () -> {
            BankSystem.getInstance().getSpigotLocationProvider().hasLocationSaved("location1", isThere -> {
                if(isThere)
                    location1 = BankSystem.getInstance().getSpigotLocationProvider().getLocationSync("location1");
                else
                    Bukkit.getLogger().info("Found unset Tresor-Location '" + "location1" + "'. Please set it with /setlocation " + BankSystem.getInstance().getDescription().getName() + " " + "location1");
            });
            BankSystem.getInstance().getSpigotLocationProvider().hasLocationSaved("location2", isThere -> {
                if(isThere)
                    location2 = BankSystem.getInstance().getSpigotLocationProvider().getLocationSync("location2");
                else
                    Bukkit.getLogger().info("Found unset Tresor-Location '" + "location2" + "'. Please set it with /setlocation " + BankSystem.getInstance().getDescription().getName() + " " + "location2");
            });
            BankSystem.getInstance().getSpigotLocationProvider().hasLocationSaved("teleportBackLocation", isThere -> {
                if(isThere)
                    teleportBackLocation = BankSystem.getInstance().getSpigotLocationProvider().getLocationSync("teleportBackLocation");
                else
                    Bukkit.getLogger().info("Found unset Tresor-Location '" + "teleportBackLocation" + "'. Please set it with /setlocation " + BankSystem.getInstance().getDescription().getName() + " " + "teleportBackLocation");
            });
        }, 20);
        Bukkit.getScheduler().runTaskTimer(BankSystem.getInstance(), () -> {
            Bukkit.getOnlinePlayers().forEach(player -> {
                if(location1 != null && location2 != null && teleportBackLocation != null){
                    if(LocationUtil.isLocationInRegion(player.getLocation(), location1, location2)){
                        player.teleport(teleportBackLocation);
                        BankSystem.getInstance().getTresorLevel().getLevel(AunaAPI.getApi().getPlayer(player.getUniqueId()), level -> {
                            AunaPlayer aunaPlayer = AunaAPI.getApi().getPlayer(player.getPlayer().getUniqueId());
                            if(level > 0)
                                new TresorGui(aunaPlayer, level).open(player.getPlayer());
                            else
                                aunaPlayer.sendMessage("bank.tresor.noAccess");
                        });
                    }
                }
            });
        }, 0, 10);
    }
}
