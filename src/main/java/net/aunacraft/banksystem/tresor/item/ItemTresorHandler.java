package net.aunacraft.banksystem.tresor.item;

import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import net.aunacraft.banksystem.BankSystem;
import net.aunacraft.banksystem.tresor.item.TresorItem;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import javax.sql.rowset.CachedRowSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

public class ItemTresorHandler {

    private static final TypeToken<List<TresorItem>> type =  new TypeToken<List<TresorItem>>(){};

    /**
     * Gets the Items from a Tresor Async
     *
     * @param uuid
     * @param listCallback
     */
    public void getItems(String uuid, Consumer<List<TresorItem>> listCallback) {
        BankSystem.getInstance().getDatabaseHandler().createBuilder("SELECT * FROM bank_tresorInv WHERE uuid = ?")
                .addObjects(uuid).queryAsync(cachedRowSet -> {
            String content = null;
            try {
                if (cachedRowSet.next()) {
                    content = cachedRowSet.getString("jsonContent");
                } else {
                    List<TresorItem> itemList = new ArrayList<>();
                    BankSystem.getInstance().getDatabaseHandler().createBuilder("INSERT INTO bank_tresorInv(uuid, jsonContent) VALUES (?, ?)").addObjects(uuid, listToString(itemList)).updateAsync();
                }
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
            List<TresorItem> itemList = fromStringToList(content);
            listCallback.accept(itemList);
        });
    }

    /**
     * Sets the Items async
     *
     * @param uuid
     * @param content
     */
    public void setItems(String uuid, List<TresorItem> content) {
        System.out.println("3");
        BankSystem.getInstance().getDatabaseHandler().createBuilder("UPDATE bank_tresorInv SET jsonContent = ? WHERE uuid = ?").addObjects(listToString(content), uuid).updateAsync();
    }

    public String listToString(List<TresorItem> itemList) {
        return new Gson().toJson(itemList, type.getType());
    }

    public List<TresorItem> fromStringToList(String content) {
        return new Gson().fromJson(content, type.getType());
    }

    /**
     * @param inventory
     * @param blockedItems
     * @return List of Tresor Items translated from Allowed and not allowed
     */
    public List<TresorItem> getItemList(final Inventory inventory, final List<ItemStack> blockedItems) {
        List<TresorItem> list = new ArrayList<>();
        int i = 0;
        for (ItemStack items : inventory.getContents()) {
            if (!blockedItems.contains(items) && inventory.getSize() - 10 > i) {
                if (items != null) {
                    list.add(new TresorItem(items, i));
                }
            }
            i += 1;
        }
        return list;
    }

}
