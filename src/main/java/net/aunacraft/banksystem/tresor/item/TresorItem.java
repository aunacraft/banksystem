package net.aunacraft.banksystem.tresor.item;

import com.google.gson.Gson;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.io.BukkitObjectInputStream;
import org.bukkit.util.io.BukkitObjectOutputStream;
import org.yaml.snakeyaml.external.biz.base64Coder.Base64Coder;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

public class TresorItem {

    private final String content;
    private final int slot;

    public TresorItem(ItemStack content, int slot) {
        this.content = itemStackToBase64(content);
        this.slot = slot;
    }

    public static String itemStackToBase64(ItemStack itemStack){
        try {
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            BukkitObjectOutputStream dataOutput = new BukkitObjectOutputStream(outputStream);
            dataOutput.writeObject(itemStack);
            dataOutput.close();
            return Base64Coder.encodeLines(outputStream.toByteArray());
        } catch (Exception e) {
            throw new IllegalStateException("Unable to save item stacks.", e);
        }
    }

    public static ItemStack itemStackFromBase64(String data) throws IOException {
        try {
            ByteArrayInputStream inputStream = new ByteArrayInputStream(Base64Coder.decodeLines(data));
            BukkitObjectInputStream dataInput = new BukkitObjectInputStream(inputStream);
            ItemStack itemStack = (ItemStack) dataInput.readObject();
            dataInput.close();
            return itemStack;
        } catch (ClassNotFoundException e) {
            throw new IOException("Unable to decode class type.", e);
        }
    }

    public static TresorItem fromString(String from) {
        return new Gson().fromJson(from, TresorItem.class);
    }

    public int getSlot() {
        return slot;
    }

    public ItemStack getContent() {
        try {
            return itemStackFromBase64(content);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public String getRawContent() {
        return content;
    }

    public String toString() {
        return new Gson().toJson(this);
    }
}
