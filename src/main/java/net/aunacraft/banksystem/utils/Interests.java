package net.aunacraft.banksystem.utils;

import java.sql.Timestamp;
import java.time.LocalDateTime;

public class Interests {

    public static final String META_NAME = "bank_last_online_time";
    private static final float INTERESTS = 0.01F;

    public static double addInterestsAfterJoining(long lastTimeOnline, double bankAmount) {
        LocalDateTime lastOnlineTime = new Timestamp(lastTimeOnline).toLocalDateTime();
        LocalDateTime timeNow = LocalDateTime.now();
        int days = timeNow.getDayOfYear() - lastOnlineTime.getDayOfYear();
        if (lastOnlineTime.getYear() != timeNow.getYear()) {
            days = +365 * (timeNow.getYear() - lastOnlineTime.getYear());
        }
        for (int i = 0; i < days; i++) {
            bankAmount = addInterests(bankAmount);
        }
        return bankAmount;
    }


    public static double addInterests(double bankAmount) {
        return Math.ceil(bankAmount * Interests.INTERESTS);
    }
}
