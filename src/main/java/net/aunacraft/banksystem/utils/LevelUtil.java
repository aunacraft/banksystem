package net.aunacraft.banksystem.utils;

public class LevelUtil {

    public static double maxAmountInBank(int bankLevel){
        return bankLevel == 1 ? 100000 : bankLevel == 2 ? 500000 : bankLevel == 3 ? 2000000 : bankLevel == 4 ? 10000000 : Double.MAX_VALUE;
    }

}
