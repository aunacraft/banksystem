package net.aunacraft.banksystem.utils;

public class MathUtil {

    public static int percent(double part, double ofAll){
        double one = ofAll / 100;
        double percent = part / one;
        return (int) (percent > 100 ? 100 : percent);
    }

    public static int getRandom(int min, int max) {
        if (min > max) throw new IllegalArgumentException("Min " + min + " greater than max " + max);
        return (int) ( (long) min + Math.random() * ((long)max - min + 1));
    }

}
